## 2.0.1 (2019-09-10)

Fixes:

  - Properly import IOClient

## 2.0.0 (2019-08-12)

Features:

  - rewrite for server side one-time payments with checkout

## 1.2.2 (2016-10-20)

Features:

  - upgrade dependencies

Bug fixes:

  - change card type to card brand


## 1.2.1 (2016-08-08)

Features:

  - add CHANGELOG.md (closes [#32](https://github.com/exitlive/stripe-dart/issues/32))
  - add default source to customer ([#33](https://github.com/exitlive/stripe-dart/pull/33), [@rbellens](https://github.com/rbellens))

Bug fixes:

  - fix card creation with token ([#33](https://github.com/exitlive/stripe-dart/pull/33), [@rbellens](https://github.com/rbellens))
